# Csv标准定义

1. 标题行可有可无，若有，必须是位于第一行
2. 一行是一条记录，记录的字段数目与标题字段数目一致
3. 字段间用英文逗号分隔
4. 字段内若含有双引号，空白字符，逗号，字段被双引号包裹
5. 字段内的双引号需要再加一个双引号转义，如 say“hello”,需是say""hello""
6. 无空行

# BestCsv使用教程

添加引用 BestCsv.dll，引入命名空间 BestCsv.

## 遍历CSV

方式一：CsvReader

```c#
using (CsvReader reader = new CsvReader(Path.GetFullPath("./info.csv")))
{
    while (reader.ReadNextRecord())
    {
        foreach (string field in reader.Fields)
        {
            Console.WriteLine(field);
        }
        Console.WriteLine();
    }
}
```

方式二：CsvFile

```csharp
CsvFile file = new CsvFile();
file.Populate(Path.GetFullPath("./info.csv"),true);
// 遍历CSV标题
foreach (string header in file.Headers)
{
    Console.WriteLine(header);
}
// 遍历记录
foreach (CsvRecord item in file.Records)
{
    foreach (string field in item.Fields)
    {
        Console.WriteLine(field);
    }
}
```

## 打印指定的行的记录的指定字段

```csharp
CsvFile file = new CsvFile();
file.Populate(Path.GetFullPath("./info.csv"), true);
Console.WriteLine($"第一行记录的mobile字段：{file[0, "mobile"]}");
Console.WriteLine($"第一行记录的第一个字段：{file[0, 1]}");
```

## 生成Csv文件

```csharp
CsvFile cf = new CsvFile();
cf.Headers.Clear();
cf.Headers.Add("name");
cf.Headers.Add("gender");
cf.Headers.Add("age");
cf.Headers.Add("mobile");
CsvRecord record = new CsvRecord();
record.Fields.Add("王二");
record.Fields.Add(true.ToString());
record.Fields.Add(18.ToString());
record.Fields.Add("13722222222");
cf.Records.Add(record);

using (CsvWriter writer = new CsvWriter())
{
    writer.WriteCsv(cf, Path.GetFullPath("./test.csv"), Encoding.GetEncoding("gbk"));
}
```

## CSV转DataTable

```csharp
using (CsvReader reader = new CsvReader(Path.GetFullPath("./info.csv")))
{
    // DataTable的列的类型是string
    DataTable dataTable = reader.ReadIntoDataTable();

    // 可指定DataTable每个列的类型
    dataTable = reader.ReadIntoDataTable(new Type[] { typeof(string), typeof(bool), typeof(int), typeof(string) });
}
```

## TrimColumns

设置成true，读取字段时，如果字段的值两端含有空白字符，会被去除。如 "  hello world",读取到的是"hello world".

```csharp
using (CsvReader reader = new CsvReader(Path.GetFullPath("./info.csv")))
{
    reader.TrimColumns = true;
    //reader.TrimColumns = false;
    while (reader.ReadNextRecord())
    {
        foreach (string field in reader.Fields)
        {
            Console.WriteLine(field);
        }
        Console.WriteLine();
    }
}
```

## 处理字段值中含有换行符的情况

 Features...

# 其他优秀的Csv库推荐
Nuget搜索 CsvHelper

[CsvHelper Wiki](https://joshclose.github.io/CsvHelper/)
