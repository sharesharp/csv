﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestCsv
{
    public sealed class CsvRecord
    {
        public readonly List<string> Fields = new List<string>();

        public int FieldCount
        {
            get
            {
                return Fields.Count;
            }
        }
    }
}
