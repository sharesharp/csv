﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestCsv.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            // 遍历CSV
            using (CsvReader reader = new CsvReader(Path.GetFullPath("./info.csv")))
            {
                reader.TrimColumns = true;
                //reader.TrimColumns = false;
                while (reader.ReadNextRecord())
                {
                    foreach (string field in reader.Fields)
                    {
                        Console.WriteLine(field);
                    }
                    Console.WriteLine();
                }
            }



            CsvFile file = new CsvFile();
            file.Populate(Path.GetFullPath("./info.csv"), true);
            Console.WriteLine($"第一行记录的mobile字段：{file[0, "mobile"]}");
            Console.WriteLine($"第一行记录的mobile字段：{file[0, 1]}");


            // 遍历CSV标题
            foreach (string header in file.Headers)
            {
                Console.WriteLine(header);
            }
            // 遍历记录
            foreach (CsvRecord item in file.Records)
            {
                foreach (string field in item.Fields)
                {
                    Console.WriteLine(field);
                }
            }

            // 创建CSV文件
            CsvFile cf = new CsvFile();
            cf.Headers.Clear();
            cf.Headers.Add("name");
            cf.Headers.Add("gender");
            cf.Headers.Add("age");
            cf.Headers.Add("mobile");
            CsvRecord record = new CsvRecord();
            record.Fields.Add("王二");
            record.Fields.Add(true.ToString());
            record.Fields.Add(18.ToString());
            record.Fields.Add("");
            cf.Records.Add(record);

            using (CsvWriter writer = new CsvWriter())
            {
                writer.WriteCsv(cf, @"C:\Users\Liuwa\Desktop\hello.csv", Encoding.GetEncoding("gbk"));
            }

          


            // CSV转DataTable
            using (CsvReader reader = new CsvReader(Path.GetFullPath("./info.csv")))
            {
                // DataTable的列的类型是string
                DataTable dataTable = reader.ReadIntoDataTable();

                // 可指定DataTable每个列的类型
                dataTable = reader.ReadIntoDataTable(new Type[] { typeof(string), typeof(bool), typeof(int), typeof(string) });

            }

            Console.ReadKey();
        }
    }
}
